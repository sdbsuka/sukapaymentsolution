package com.sdb.sukapaymentsolution.Db;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import java.util.Date;

@Entity(tableName = "creditCards")
public class CreditCardsStorage {

    @PrimaryKey(autoGenerate = true)
    private int _id;
    private int creditCardId;
    private int bankId;

    private String holderName;
    private String billingAddress;
    private String cities;
    private String postCode;
    private String phone;

    private String cardNumber;

    private String expDate;

    public CreditCardsStorage(int id,
                              int creditCardId,
                              int bankId,
                              String holderName,
                              String billingAddress,
                              String cities,
                              String postCode,
                              String phone,
                              String cardNumber,
                              String expDate) {
        _id = id;
        this.creditCardId = creditCardId;
        this.bankId = bankId;
        this.holderName = holderName;
        this.billingAddress = billingAddress;
        this.cities = cities;
        this.postCode = postCode;
        this.phone = phone;
        this.cardNumber = cardNumber;
        this.expDate = expDate;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public int getCreditCardId() {
        return creditCardId;
    }

    public void setCreditCardId(int creditCardId) {
        this.creditCardId = creditCardId;
    }

    public int getBankId() {
        return bankId;
    }

    public void setBankId(int bankId) {
        this.bankId = bankId;
    }

    public String getHolderName() {
        return holderName;
    }

    public void setHolderName(String holderName) {
        this.holderName = holderName;
    }

    public String getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(String billingAddress) {
        this.billingAddress = billingAddress;
    }

    public String getCities() {
        return cities;
    }

    public void setCities(String cities) {
        this.cities = cities;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getExpDate() {
        return expDate;
    }

    public void setExpDate(String expDate) {
        this.expDate = expDate;
    }
}
