package com.sdb.sukapaymentsolution.Db;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.sdb.sukapaymentsolution.data.Token;

@Entity(tableName = "Credentials",
        indices = {@Index("_id")})
public class CredentialStorage {

    @PrimaryKey(autoGenerate = true)
    private int _id;

    @NonNull
    private String _encodedToken;

    @NonNull
    private String _note;

    public CredentialStorage(){}

    public CredentialStorage(Token token, String note){

        _encodedToken = token.toString();
        _note = note;
    }

    @NonNull
    public String get_note() {
        return _note;
    }

    public void set_note(@NonNull String _note) {
        this._note = _note;
    }

    @NonNull
    public String get_encodedToken() {
        return _encodedToken;
    }

    public void set_encodedToken(@NonNull String _encodedToken) {
        this._encodedToken = _encodedToken;
    }

    @NonNull
    public int get_id() {
        return _id;
    }

    public void set_id(@NonNull int _id) {
        this._id = _id;
    }
}
