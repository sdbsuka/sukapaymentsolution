package com.sdb.sukapaymentsolution.Db;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "ms_Banks", indices = {@Index("_id")})
public class MasterBankStorage {

    @PrimaryKey(autoGenerate = true)
    private int _id;
    @NonNull
    private String _namaBank ="";
    @NonNull
    private String _namaPemegang = "";
    @NonNull
    private String _nomorRekening = "";

    //region getter and setter
    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    @NonNull
    public String get_namaBank() {
        return _namaBank;
    }

    public void set_namaBank(@NonNull String _namaBank) {
        this._namaBank = _namaBank;
    }

    @NonNull
    public String get_namaPemegang() {
        return _namaPemegang;
    }

    public void set_namaPemegang(@NonNull String _namaPemegang) {
        this._namaPemegang = _namaPemegang;
    }

    @NonNull
    public String get_nomorRekening() {
        return _nomorRekening;
    }

    public void set_nomorRekening(@NonNull String _nomorRekening) {
        this._nomorRekening = _nomorRekening;
    }
    //endregion
}
