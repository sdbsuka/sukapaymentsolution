package com.sdb.sukapaymentsolution.Db;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "ms_CreditCard", indices = {@Index("_id")})
public class MasterCreditCardStorage {

    @PrimaryKey(autoGenerate = true)
    private int _id;

    private int _creditCardId =-1;
    private int _bankId = -1;

    @NonNull
    private String _name ="";
    @NonNull
    private String _subName = "";

    public int get_creditCardId() {
        return _creditCardId;
    }

    public void set_creditCardId(int _creditCardId) {
        this._creditCardId = _creditCardId;
    }

    public int get_bankId() {
        return _bankId;
    }

    public void set_bankId(int _bankId) {
        this._bankId = _bankId;
    }

    @NonNull
    public String get_name() {
        return _name;
    }

    public void set_name(@NonNull String _name) {
        this._name = _name;
    }

    @NonNull
    public String get_subName() {
        return _subName;
    }

    public void set_subName(@NonNull String _subName) {
        this._subName = _subName;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }
}
