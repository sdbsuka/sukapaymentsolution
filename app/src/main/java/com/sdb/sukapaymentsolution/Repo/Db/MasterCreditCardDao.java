package com.sdb.sukapaymentsolution.Repo.Db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.sdb.sukapaymentsolution.Db.CredentialStorage;
import com.sdb.sukapaymentsolution.Db.MasterCreditCardStorage;
import com.sdb.sukapaymentsolution.data.CreditCardModel;

@Dao
public interface MasterCreditCardDao {

    @Insert
    void insert(MasterCreditCardStorage creditCardStorage);

    @Delete
    void delete(MasterCreditCardStorage creditCardStorage);

    @Update
    void update(MasterCreditCardStorage creditCardUpdate);

    @Query("Select * from ms_CreditCard where _id=:id")
    MasterCreditCardStorage get(int id);
}
