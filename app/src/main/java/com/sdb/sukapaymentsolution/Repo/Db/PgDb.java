package com.sdb.sukapaymentsolution.Repo.Db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.sdb.sukapaymentsolution.Db.CredentialStorage;
import com.sdb.sukapaymentsolution.Db.MasterBankStorage;
import com.sdb.sukapaymentsolution.Db.MasterCreditCardStorage;
import com.sdb.sukapaymentsolution.Db.CreditCardsStorage;


@Database(entities = {
        CredentialStorage.class,
        MasterBankStorage.class,
        MasterCreditCardStorage.class,
        CreditCardsStorage.class
}, version = 1)
public abstract class PgDb extends RoomDatabase {

    private static PgDb _Instance;

    public static PgDb getDatabase(final Context context){
        if(_Instance == null)
        {
            synchronized (PgDb.class){
                if(_Instance == null){
                    _Instance = Room.databaseBuilder(context.getApplicationContext(),PgDb.class,"pgdb")
                            .build();
                }
            }
        }
        return _Instance;
    }

    public abstract MasterCreditCardDao MasterCreditCard();
    public abstract MasterBankStorageDao MasterBank();
    public abstract CredentialDao Credentials();
    public abstract CreditCardDao CreditCard();

}
