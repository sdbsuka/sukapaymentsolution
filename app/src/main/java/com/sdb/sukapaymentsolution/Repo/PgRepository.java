package com.sdb.sukapaymentsolution.Repo;

import android.app.Application;
import android.os.AsyncTask;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sdb.sukapaymentsolution.AppManager;
import com.sdb.sukapaymentsolution.Db.CredentialStorage;
import com.sdb.sukapaymentsolution.Db.MasterBankStorage;
import com.sdb.sukapaymentsolution.Db.MasterCreditCardStorage;
import com.sdb.sukapaymentsolution.Repo.Db.MasterBankStorageDao;
import com.sdb.sukapaymentsolution.Repo.Db.CredentialDao;
import com.sdb.sukapaymentsolution.Repo.Db.MasterCreditCardDao;
import com.sdb.sukapaymentsolution.Repo.Db.CreditCardDao;
import com.sdb.sukapaymentsolution.Repo.Db.PgDb;
import com.sdb.sukapaymentsolution.data.CreditCardModel;
import com.sdb.sukapaymentsolution.data.RegisterModel;
import com.sdb.sukapaymentsolution.data.UserBankModel;

import org.json.JSONObject;

public class PgRepository {

    private final String Base_Url = "http://10.0.2.2:5000/api";
    private Gson gsonObject ;

    //real time
    private CredentialDao credentialsDao;
    private CreditCardDao creditCardDao;

    //master
    private MasterCreditCardDao masterCreditCardDao;
    private MasterBankStorageDao masterBankDao;

    public PgRepository(Application app) {

        PgDb db = PgDb.getDatabase(app.getApplicationContext());

        credentialsDao = db.Credentials();
        masterBankDao = db.MasterBank();
        creditCardDao = db.CreditCard();
        masterCreditCardDao = db.MasterCreditCard();

        gsonObject = AppManager.getInstance().getGsonObject();
    }

    public void RegisterUser(RegisterModel model){
        String toJsonString = model.toString();
        JSONObject jsO = (JSONObject)gsonObject.fromJson(toJsonString, JSONObject.class);

        Response.Listener<JSONObject> success = new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

            }
        };
        Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        };

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,
                "usm/register",
                jsO,
                success,
                error){

            @Override
            public String getBodyContentType() {
                return "application/json;charset=utf-8";
            }
        };

        AppManager.getInstance().addToRequestQueue(request);
    }

    public void SyncMasterKartu(){
    }

    public void SynchMasterBank(){
    }

    public void SaveKartuBaru(CreditCardModel creditCardInfo){
    }

    public void UpdateKartu(int idKartu, CreditCardModel dataCCUpdate){
    }

    public void DeleteKartu(int idKartu){
    }

    public void SimpanDataBankBaru(UserBankModel dataBank){
    }

    public void UpdateBank(int dataBank, UserBankModel dataBankUpdate){
    }

    public void DeleteBank(int dataBank){
    }


    //region asyncTask db

    private static class InsertMasterCCAsyncTask extends AsyncTask<MasterCreditCardStorage, Void, Void> {

        private MasterCreditCardDao asyncTaskMasterCCDao;

        InsertMasterCCAsyncTask(MasterCreditCardDao masterCCDao){
            asyncTaskMasterCCDao = masterCCDao;
        }

        @Override
        protected Void doInBackground(MasterCreditCardStorage... masterCreditCardStorages) {
            asyncTaskMasterCCDao.insert(masterCreditCardStorages[0]);
            return null;
        }
    }

    private static class InsertMasterBankAsyncTask extends AsyncTask<MasterBankStorage, Void, Void>{

        private  MasterBankStorageDao asyncTaskMasterBankDao;

        InsertMasterBankAsyncTask(MasterBankStorageDao masterBankStorageDao){
            asyncTaskMasterBankDao = masterBankStorageDao;
        }

        @Override
        protected Void doInBackground(MasterBankStorage... masterBankStorages) {
            asyncTaskMasterBankDao.insert(masterBankStorages[0]);
            return null;
        }
    }

    private static class InsertCredentialAsyncTask extends AsyncTask<CredentialStorage, Void, Void>{

        private final CredentialDao _credentialDao;

        InsertCredentialAsyncTask(CredentialDao credentialDao){
            _credentialDao = credentialDao;
        }


        @Override
        protected Void doInBackground(CredentialStorage... credentialStorages) {
            _credentialDao.insert(credentialStorages[0]);
            return null;
        }
    }



    //endregion

}
