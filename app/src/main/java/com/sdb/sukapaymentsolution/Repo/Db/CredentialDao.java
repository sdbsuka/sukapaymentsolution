package com.sdb.sukapaymentsolution.Repo.Db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.sdb.sukapaymentsolution.Db.CredentialStorage;
import com.sdb.sukapaymentsolution.data.Token;

import java.util.List;

@Dao
public interface CredentialDao {

    @Insert
    void insert(CredentialStorage credential);

    @Delete
    void delete(CredentialStorage credential);

    @Update
    void update(CredentialStorage credentialUpdate);

    @Query("Select * from Credentials where _id=:id")
    CredentialStorage get(int id);

}
