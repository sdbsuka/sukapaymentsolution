package com.sdb.sukapaymentsolution.Repo.Db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.sdb.sukapaymentsolution.Db.CredentialStorage;
import com.sdb.sukapaymentsolution.Db.CreditCardsStorage;
import com.sdb.sukapaymentsolution.data.CreditCardModel;

import java.util.List;

@Dao
public interface CreditCardDao {

    @Insert
    void insert(CreditCardsStorage ccData);

    @Delete
    void delete(CreditCardsStorage ccData);

    @Update
    void update(CreditCardsStorage ccData);

    @Query("Select * from creditCards")
    LiveData< List<CreditCardsStorage>> allOwnedCreditCards();
}
