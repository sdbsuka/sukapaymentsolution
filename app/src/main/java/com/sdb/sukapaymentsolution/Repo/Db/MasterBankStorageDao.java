package com.sdb.sukapaymentsolution.Repo.Db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.sdb.sukapaymentsolution.Db.MasterBankStorage;
import com.sdb.sukapaymentsolution.data.UserBankModel;

import java.util.List;

@Dao
public interface MasterBankStorageDao {

    @Insert
    void insert(MasterBankStorage masterBankData);

    @Delete
    void delete(MasterBankStorage masterBankData);

    @Update()
    void update(MasterBankStorage masterBankUpdate);

    @Query("Select * from ms_Banks")
    LiveData<List<MasterBankStorage>> GetAllBanks();
}
