package com.sdb.sukapaymentsolution;

import android.location.LocationManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.SettingsClient;
import com.karumi.dexter.Dexter;
import com.sdb.sukapaymentsolution.Repo.PgRepository;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.btnInfo)
    Button btnInfo;

    //region helper fields
    private LocationCallback locationCallback;
    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //bind butterknife
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btnInfo)
    public void GetInfo(){

        locationCallback = new LocationCallback(){
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
            }
        };

        AppManager.getInstance().initLocation(locationCallback,
                AppManager.UPDATE_INTERVAL_IN_MILLISECONDS,
                AppManager.FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS, false);

    }
}
