package com.sdb.sukapaymentsolution.data;

import com.sdb.sukapaymentsolution.AppManager;

public class RegisterModel  {

    private boolean isConfirmedByMail;
    private int platform;
    private String timeStamp;
    private String password;
    private String phoneNumber;
    private String ipAddress;
    private String userEmail;
    private String userName;
    private String platformUniqueId;

    @Override
    public String toString() {
        return AppManager.getInstance().getGsonObject().toJson(this);
    }

    public RegisterModel() {}
    public RegisterModel(String timeStamp,
                         String password,
                         String phoneNumber,
                         boolean  isConfirmedByMail,
                         String ipAddress,
                         String userEmail,
                         String userName,
                         int platform,
                         String platformUniqueId){

        this.timeStamp = timeStamp;
        this.password = password;
        this.phoneNumber = phoneNumber;
        this.isConfirmedByMail = isConfirmedByMail;
        this.ipAddress = ipAddress;
        this.userEmail = userEmail;
        this.userName = userName;
        this.platform = platform;
        this.platformUniqueId = platformUniqueId;
    }
    public String getTimeStamp() {
		return timeStamp;
	}
	
    public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}
    
    public String getPassword() {
		return password;
	}
	
    public void setPassword(String password) {
		this.password = password;
	}
    
    public String getPhoneNumber() {
		return phoneNumber;
	}
	
    public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
    
    public String getIpAddress() {
		return ipAddress;
	}
	
    public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
    
    public String getUserEmail() {
		return userEmail;
	}
	
    public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}
    
    public String getUserName() {
		return userName;
	}
	
    public void setUserName(String userName) {
		this.userName = userName;
	}
    
    public String getPlatformUniqueId() {
		return platformUniqueId;
	}
	
    public void setPlatformUniqueId(String platformUniqueId) {
        this.platformUniqueId = platformUniqueId;
    }
    
    public int getPlatform() {
		return platform;
	}
	
    public void setPlatform(int platform) {
		this.platform = platform;
	}
    
    public boolean getIsConfirmedByMail() {
		return isConfirmedByMail;
	}
	
    public void setIsConfirmedByMail(boolean isConfirmedByMail) {
        this.isConfirmedByMail = isConfirmedByMail;
    }
    
}