package com.sdb.sukapaymentsolution.data;

import java.util.Date;

public class UserBalanceModel {

    private Date    date;
    private double  balance;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }
}
