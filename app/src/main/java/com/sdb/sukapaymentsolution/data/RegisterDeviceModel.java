package com.sdb.sukapaymentsolution.data;
public class RegisterDeviceModel  {
    private boolean isPrimaryDevice;
    private int latitude;
    private int platform;
    private int longitude;
    private String password;
    private String nomorTelepon;
    private String label;
    private String userName;
    private String uniquePlatformId;
    
    public RegisterDeviceModel() {}

    public RegisterDeviceModel(String password, int latitude, boolean  isPrimaryDevice, String nomorTelepon, String label, String userName, int platform, String uniquePlatformId, int longitude){
        this.password = password;
    
        this.latitude = latitude;
    
        this.isPrimaryDevice = isPrimaryDevice;
    
        this.nomorTelepon = nomorTelepon;
    
        this.label = label;
    
        this.userName = userName;
    
        this.platform = platform;
    
        this.uniquePlatformId = uniquePlatformId;
    
        this.longitude = longitude;
    
    }
    public String getPassword() {
		return password;
	}
	
    public void setPassword(String password) {
		this.password = password;
	}
    
    public String getNomorTelepon() {
		return nomorTelepon;
	}
	
    public void setNomorTelepon(String nomorTelepon) {
		this.nomorTelepon = nomorTelepon;
	}
    
    public String getLabel() {
		return label;
	}
	
    public void setLabel(String label) {
		this.label = label;
	}
    
    public String getUserName() {
		return userName;
	}
	
    public void setUserName(String userName) {
		this.userName = userName;
	}
    
    public String getUniquePlatformId() {
		return uniquePlatformId;
	}
	
    public void setUniquePlatformId(String uniquePlatformId) {
        this.uniquePlatformId = uniquePlatformId;
    }
    
    public int getLatitude() {
		return latitude;
	}
	
    public void setLatitude(int latitude) {
		this.latitude = latitude;
	}
    
    public int getPlatform() {
		return platform;
	}
	
    public void setPlatform(int platform) {
		this.platform = platform;
	}
    
    public int getLongitude() {
		return longitude;
	}
	
    public void setLongitude(int longitude) {
		this.longitude = longitude;
	}
    
    public boolean getIsPrimaryDevice() {
		return isPrimaryDevice;
	}
	
    public void setIsPrimaryDevice(boolean isPrimaryDevice) {
        this.isPrimaryDevice = isPrimaryDevice;
    }
    
}