package com.sdb.sukapaymentsolution.data;

import java.util.Date;

public class LoginModel {

    private String userName;
    private String password;
    private String platformUniqueId;
    private double longitude ;
    private double latitude;
    private Date timeStamp;
    private String ipAddress ;


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPlatformUniqueId() {
        return platformUniqueId;
    }

    public void setPlatformUniqueId(String platformUniqueId) {
        this.platformUniqueId = platformUniqueId;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }
}
