package com.sdb.sukapaymentsolution.data;

public class UserBankModel {

    private String id ;
    private String namaBank ;
    private String namaPemegang;
    private String nomorRekening ;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNamaBank() {
        return namaBank;
    }

    public void setNamaBank(String namaBank) {
        this.namaBank = namaBank;
    }

    public String getNamaPemegang() {
        return namaPemegang;
    }

    public void setNamaPemegang(String namaPemegang) {
        this.namaPemegang = namaPemegang;
    }

    public String getNomorRekening() {
        return nomorRekening;
    }

    public void setNomorRekening(String nomorRekening) {
        this.nomorRekening = nomorRekening;
    }
}
