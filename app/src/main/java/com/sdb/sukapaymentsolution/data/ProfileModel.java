package com.sdb.sukapaymentsolution.data;

public class ProfileModel {

    private int jenisKelamin;

    private String provinsi;

    private String kota;

    private String nomorHandPhone;

    private String kodePos;

    private String id;

    private String tanggalLahir;

    private String namaAwal;

    private String user;

    private String namaAkhir;

    private String alamat;

    public ProfileModel() {
    }

    public ProfileModel(String provinsi,
                        String kota,
                        String nomorHandPhone,
                        int jenisKelamin,
                        String kodePos,
                        String id,
                        String tanggalLahir,
                        String namaAwal,
                        String user,
                        String namaAkhir,
                        String alamat) {
        this.provinsi = provinsi;

        this.kota = kota;

        this.nomorHandPhone = nomorHandPhone;

        this.jenisKelamin = jenisKelamin;

        this.kodePos = kodePos;

        this.id = id;

        this.tanggalLahir = tanggalLahir;

        this.namaAwal = namaAwal;

        this.user = user;

        this.namaAkhir = namaAkhir;

        this.alamat = alamat;

    }

    public String getProvinsi() {
        return provinsi;
    }

    public void setProvinsi(String provinsi) {
        this.provinsi = provinsi;
    }

    public String getKota() {
        return kota;
    }

    public void setKota(String kota) {
        this.kota = kota;
    }

    public String getNomorHandPhone() {
        return nomorHandPhone;
    }

    public void setNomorHandPhone(String nomorHandPhone) {
        this.nomorHandPhone = nomorHandPhone;
    }

    public String getKodePos() {
        return kodePos;
    }

    public void setKodePos(String kodePos) {
        this.kodePos = kodePos;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTanggalLahir() {
        return tanggalLahir;
    }

    public void setTanggalLahir(String tanggalLahir) {
        this.tanggalLahir = tanggalLahir;
    }

    public String getNamaAwal() {
        return namaAwal;
    }

    public void setNamaAwal(String namaAwal) {
        this.namaAwal = namaAwal;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getNamaAkhir() {
        return namaAkhir;
    }

    public void setNamaAkhir(String namaAkhir) {
        this.namaAkhir = namaAkhir;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public int getJenisKelamin() {
        return jenisKelamin;
    }

    public void setJenisKelamin(int jenisKelamin) {
        this.jenisKelamin = jenisKelamin;
    }

}