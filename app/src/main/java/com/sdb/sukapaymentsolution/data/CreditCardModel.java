package com.sdb.sukapaymentsolution.data;

import java.util.Date;

public class CreditCardModel {

    private int CreditCardId;
    private int BankId;

    private String HolderName;
    private String BillingAddress;
    private String Cities;
    private String PostCode;
    private String Phone;

    private String CardNumber;

    private String ExpDate;

    public int getCreditCardId() {
        return CreditCardId;
    }

    public void setCreditCardId(int creditCardId) {
        CreditCardId = creditCardId;
    }

    public int getBankId() {
        return BankId;
    }

    public void setBankId(int bankId) {
        BankId = bankId;
    }

    public String getHolderName() {
        return HolderName;
    }

    public void setHolderName(String holderName) {
        HolderName = holderName;
    }

    public String getBillingAddress() {
        return BillingAddress;
    }

    public void setBillingAddress(String billingAddress) {
        BillingAddress = billingAddress;
    }

    public String getCities() {
        return Cities;
    }

    public void setCities(String cities) {
        Cities = cities;
    }

    public String getPostCode() {
        return PostCode;
    }

    public void setPostCode(String postCode) {
        PostCode = postCode;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getCardNumber() {
        return CardNumber;
    }

    public void setCardNumber(String cardNumber) {
        CardNumber = cardNumber;
    }

    public String getExpDate() {
        return ExpDate;
    }

    public void setExpDate(String expDate) {
        ExpDate = expDate;
    }
}
