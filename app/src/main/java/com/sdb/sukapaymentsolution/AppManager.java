package com.sdb.sukapaymentsolution;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.location.LocationManager;
import android.os.Looper;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnCanceledListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sdb.sukapaymentsolution.Repo.PgRepository;

public class AppManager extends Application {

    //region constant
    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = 5000;
    //endregion

    //region helper
    private LocationManager _locationManager;
    private PgRepository    _repo;
    //endregion

    //region singleton class
    private static AppManager _instance = null;
    public  static final String TAG = AppManager.class.getSimpleName();

    public static synchronized AppManager getInstance(){
        if(_instance == null)
            _instance = new AppManager();

        return _instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        _instance = this;

    }
    //endregion

    //region repo
    public PgRepository getRepository(){
        if(_repo == null)
            _repo = new PgRepository(this);

        return _repo;
    }
    //endregion

    //region volley
    private RequestQueue _mainRequestQueue = null;

    public RequestQueue getRequest(){

        if(_mainRequestQueue == null)
            _mainRequestQueue = Volley.newRequestQueue(getApplicationContext());

        return _mainRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequest().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequest().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (_mainRequestQueue != null) {
            getRequest().cancelAll(tag);
        }
    }
    //endregion

    //region Gson
    public Gson getGsonObject(){
        GsonBuilder builder= new GsonBuilder()
                .setDateFormat("dd/MM/yyyy hh:mm")
                .setPrettyPrinting();

        return builder.create();
    }
    //endregion

    //region location manager
    public LocationManager getLocationManager(){
        if(_locationManager == null)
            _locationManager = (LocationManager) this.getApplicationContext().getSystemService(LOCATION_SERVICE);

        return _locationManager;
    }
    //endregion

    //region location service, from gms

    @SuppressLint("MissingPermission")
    public FusedLocationProviderClient initLocation(final LocationCallback callback,
                                                    long UpdateInterval,
                                                    long FastestUpdateInterval,
                                                    boolean highPriority ){

        final FusedLocationProviderClient client = LocationServices.getFusedLocationProviderClient(getApplicationContext());

        //Location Request Setting
        final LocationRequest request = new LocationRequest();
        request.setInterval(UpdateInterval);
        request.setFastestInterval(FastestUpdateInterval);
        int priority = highPriority ? LocationRequest.PRIORITY_HIGH_ACCURACY : LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY;
        request.setPriority(priority);

        //location setting request
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(request);

        LocationSettingsRequest requestSetting = builder.build();

        //setting client
        SettingsClient settingsClient = LocationServices.getSettingsClient(getApplicationContext());
        settingsClient.checkLocationSettings(requestSetting)
                .addOnSuccessListener(new OnSuccessListener<LocationSettingsResponse>() {
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                        client.requestLocationUpdates(request, callback, Looper.myLooper());
                    }
                })
                .addOnCanceledListener(new OnCanceledListener() {
                    @Override
                    public void onCanceled() {

                    }
                })
        ;

        return client;
    }
    //endregion
}
